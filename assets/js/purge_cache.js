jQuery( document ).ready(function($) {
    $('#korndev-clearCache').click( function (event) {
        event.preventDefault();

        var button = $(this),
            processing = $( ".korndev-processing"),
            feedback = $(".korndev-cache-feedback p"),
            data =  {
                action :	'korndev_hosting_purgeCache',
                'cache_nonce' : KornDevHostingCache.ajax_cache_nonce
            };

        button.hide();
        processing.show();

        $.post( ajaxurl, data, function(response){
            feedback.html('<strong>'+ response.message +'</strong>');
        }).always(function (){
            processing.hide();
       }).done(function(){
       });
   });
});