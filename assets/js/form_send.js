jQuery( document ).ready(function($) {
		$( "#korndev-support" ).submit(function(event) {
			event.preventDefault();

			var support_nonce = $("")
				 author_name = $("#korndev_author"),
				 message_title = $("#korndev_title"),
				 message_content = $("#korndev_content"),
				 author_email = $("#korndev_author_email"),
				 install = $("#korndev_install"),
				 sending = $( ".korndev-sending"),
				 feedback = $(".korndev-support-feedback p");

			sending.show();
			feedback.html('Attempting to send your message...');

			if(message_content.val() == "" || message_title.val() == "") {
				feedback.html('Invalid details provided.');
				if (message_content.val() == ""){
					message_content.css({"border-color": "red"});
				}
				if (message_title.val() == ""){
					message_title.css({"border-color": "red"});
				}
				sending.hide();
			}
			else {
				var data =  {
					action :	'korndev_hosting_supportMessage',
					'support_nonce' : KornDevHostingSupport.ajax_support_nonce,
					'install': install.val(),
					'author_name': author_name.val(),
					'author_email': author_email.val(),
					'subject': message_title.val(),
					'message': message_content.val()
				};
				message_content.removeAttr('style');
				message_title.removeAttr('style');
				var posting = $.post( ajaxurl, data, function(response){
					feedback.html(response.message);
					if(response.success){
                        message_title.val("");
                        message_content.val("");
					}
				}).always(function (){
					sending.hide();
					setTimeout( function(){
						feedback.fadeOut(500,function(){
							$(this).html("");
							$(this).show();
						});
					},10000 );
				}).done(function(){});
			}
			return false;
	});
});