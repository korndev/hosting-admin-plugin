<?php
/*
  Plugin Name: KornDev Hosting Administrator
  Description: Watchdog Studio (from KornDev) Hosting services and setup
  Author: KornDev
  Version: 4.1.16
*/

/*
 * TODO:
 * Add settings page for setting Admins (vs adding them to wp-config)
 * Add setting to hide ACF from non super admin users
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Bootstrap class.
 *
 * A simple class to get the plugin rolling. Handles activation / deactivation related functionality.
 */
class KornDevHosting_Bootstrap {
    /**
     * Bootstrap.
     */
    public static function init() {
        // Load and instantiate the core plugin class, this class initializes everything
        require_once trailingslashit( plugin_dir_path( __FILE__ ) ) . 'includes/Plugin.php';
        \korndev\hosting\Plugin::getInstance();
    }

    /**
     * Activation
     */
    static function activate(){}

    /**
     * Deactivation
     */
    static function deactivate(){
        // Remove the options from the DB
        delete_option(\korndev\hosting\Settings::getInstance()->reset());
    }

    /**
     * Uninstall and Cleanup
     */
    static function uninstall(){}
}

add_action('plugins_loaded', function(){
    KornDevHosting_Bootstrap::init();
});

// Enable/Allow Plugin updates via WordPress updates
require trailingslashit( plugin_dir_path( __FILE__ ) ) . 'resources/plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://json.korndev.com/korndev-plugins/hosting-admin/korndev-hosting-admin.json',
    __FILE__,
    'korndev_hosting_admin'
);

/**
 *
 * Disable password change notifications
 * @since 1.0
 * @since 1.8.0 Add multisite check
 */
if (!is_multisite() && !function_exists('wp_password_change_notification')) {
    function wp_password_change_notification() {}
}

register_activation_hook( __FILE__, array( 'KornDevHosting_Bootstrap', 'activate' ) );
register_deactivation_hook(__FILE__, array( 'KornDevHosting_Bootstrap', 'deactivate' ) );
register_uninstall_hook(__FILE__, array( 'KornDevHosting_Bootstrap', 'uninstall' ) );