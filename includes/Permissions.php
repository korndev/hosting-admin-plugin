<?php

namespace korndev\hosting;

class Permissions {

    /** @var Properties Static property to hold our singleton instance  */
    static $instance = null;

    /**
     * This is our constructor
     */
    private function __construct() {}

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return Permissions
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Get The default super admin users
     *
     * @param string $login     Provide the login if you'd like to get a specific login details
     *
     * @return array|false      False is returned only when $login is provided and is not a valid default login
     */
    public function getDefaultSuperUsers( $login = '' ){
        $users = [
            Properties::getInstance()->get(Properties::KEY_ADMIN_USER) => [
                'email' => Properties::getInstance()->get(Properties::KEY_ADMIN_EMAIL),
                'first_name' => 'Justin',
                'last_name' => 'Korn',
                'nickname' => 'Hosting Admin',
                'display_name' => 'KornDev Admin'
            ],
            Properties::getInstance()->get(Properties::KEY_SUPPORT_USER) => [
                'email' => Properties::getInstance()->get(Properties::KEY_SUPPORT_EMAIL),
                'first_name' => 'KornDev',
                'last_name' => 'Support',
                'nickname' => 'Hosting Support',
                'display_name' => 'KornDev Support'
            ],
            Properties::getInstance()->get(Properties::KEY_CARE_USER) => [
                'email' => Properties::getInstance()->get(Properties::KEY_CARE_EMAIL),
                'first_name' => 'KornDev',
                'last_name' => 'Care',
                'nickname' => 'KornDev Care',
                'display_name' => 'KornDev Care'
            ]
        ];

        if( $login ){
            if( isset($users[$login]) ) {
                return $users[$login];
            }

            return false;
        }

        return $users;
    }

    /**
     * Make sure KornDev admin and support users are added if they don't exist
     */
    function setupDefaultAdmins(){
        $users = $this->getDefaultSuperUsers();

        foreach( $users as $user_login => $user_details ) {
            $user = get_user_by( 'login', $user_login );
            if ( ! $user ):
                $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
                $user_id         = wp_create_user($user_login, $random_password, $user_details['email']);
                if (is_multisite()):
                    grant_super_admin($user_id);
                endif;
            elseif (is_multisite() && ! is_super_admin($user->ID)):
                grant_super_admin($user->ID);
                wp_update_user([
                    'ID'           => $user->ID,
                    'user_url'     => Properties::getInstance()->get(Properties::KEY_HOSTING_URL),
                    'user_email'   => $user_details['email'],
                    'first_name'   => $user_details['first_name'],
                    'last_name'    => $user_details['last_name'],
                    'nickname'     => $user_details['nickname'],
                    'display_name' => $user_details['display_name']
                ]);
            elseif ( ! $user->has_cap('edit_users')):
                wp_update_user([
                    'ID'           => $user->ID,
//                    'user_url'     => Properties::getInstance()->get(Properties::KEY_SUPPORT_URL),
                    'user_email'   => $user_details['email'],
                    'role'         => 'administrator',
                    'first_name'   => $user_details['first_name'],
                    'last_name'    => $user_details['last_name'],
                    'nickname'     => $user_details['nickname'],
                    'display_name' => $user_details['display_name']
                ]);
            endif;
        }
    }

    /**
     * Check if logged in user is a Super User
     *
     * @param bool $ignore_wpconfig     If set to true, ignore any users set in the WP Config file. Default is false.
     * @param bool $korndev_only        If set to true, ignore all users except the korndev super admin user. Default is false.
     * @return bool
     *
     * @since 1.7
     */
    public function isSuperUser( $ignore_wpconfig = false, $korndev_only = false ){
        $current_user = wp_get_current_user();
        $username = $current_user->user_login;

        $superusers = array_keys( $this->getDefaultSuperUsers() );
        if( ! $korndev_only ){
            if( Properties::PROVIDER_WPENGINE == Properties::getInstance()->get(Properties::KEY_PROVIDER) ) {
                $superusers[] = 'wpengine';
            }

            if(!$ignore_wpconfig):
                if( defined('KORNDEV_HOSTING_SUPER_USERS') ):
                    $superusers = array_merge( $superusers, explode(',', KORNDEV_HOSTING_SUPER_USERS) );
                endif;
            endif;
        }

        if( in_array($username, $superusers) ):
            return true;
        endif;

        return false;
    }
}
