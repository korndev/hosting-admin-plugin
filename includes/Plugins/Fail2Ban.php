<?php

namespace korndev\hosting\Plugins;

class Fail2Ban {
    /** @var Fail2Ban Static property to hold our singleton instance */
    static $instance = null;

    /**
     * This is our constructor
     */
    private function __construct() {
    }

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return Fail2Ban
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function init(){
        add_filter('fs_show_trial_wp-fail2ban', '__return_false');
        add_filter('fs_show_affiliate_program_notice_wp-fail2ban', '__return_false');

//        add_action('wp_loaded', [$this, 'disableOptin']);
    }

    public function disableOptin(){
        if( ! defined('WP_FAIL2BAN_VER') ){
            return;
        }

        global $wf_fs;
        $wf_fs->_stroage->sticky_option_added = false;
    }
}