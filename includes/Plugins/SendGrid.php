<?php

namespace korndev\hosting\Plugins;

use korndev\hosting\Properties;

class SendGrid
{
    /** @var SendGrid Static property to hold our singleton instance */
    static $instance = null;

    /**
     * This is our constructor
     */
    private function __construct(){}

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return SendGrid
     */
    public static function getInstance()
    {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Check if the SendGrid integration on GridPane is active
     *
     * @return bool
     */
    public function isSendGridActive(){
        try {
            if(
                file_exists(trailingslashit(dirname(ABSPATH, 1)) . 'sendgrid-wp-configs.php')   // Specific to GridPane
                && defined('SMTP_HOST') && strpos(SMTP_HOST, 'sendgrid'))
            {
                return true;
            }
        } catch( \Exception $e ){
            return false;
        }

        return false;
    }

    /**
     * Set our own default name and email to send email from
     *
     * @param \PHPMailer $mailer
     */
    public function phpMailer( $mailer ){
        // Check if SendGrid is being used
        if( $this->isSendGridActive() ) {

            if( !is_object($mailer) ){
                $mailer = (object)$mailer;
            }

            $mailer->Mailer     = 'smtp';
            $mailer->Host       = SMTP_HOST;
            $mailer->SMTPAuth   = SMTP_AUTH;
            $mailer->Port       = SMTP_PORT;
            $mailer->Username   = SMTP_USER;
            $mailer->Password   = SMTP_PASS;
            $mailer->SMTPSecure = SMTP_SECURE;

            $sgHeaders = [
                'category' => Properties::getInstance()->get(Properties::KEY_DEFAULT_SENDING_DOMAIN),
                'unique_args' => [
                    'sent_by_email' => $mailer->From,
                    'sent_by_name' => $mailer->FromName,
                    'sent_host' => Properties::getInstance()->get(Properties::KEY_DEFAULT_SENDING_DOMAIN)
                ]
            ];
            $mailer->addCustomHeader('X-SMTPAPI', json_encode($sgHeaders) );

            // Always make sure emails are sending with the host domain
            Properties::getInstance()->setPHPMailer( $mailer );
        }
    }
}