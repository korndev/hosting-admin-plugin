<?php
namespace korndev\hosting\Plugins;

use korndev\hosting\Settings;

class ProjectHuddle {

    CONST OPTIONS = 'rt_wp_nginx_helper_options';

    /** @var ProjectHuddle Static property to hold our singleton instance  */
    static $instance = null;

    /**
     * This is our constructor
     */
    private function __construct() {}

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return ProjectHuddle
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Set default settings
     *
     * @return void
     */
    function setDefaults(){
        $hosting_settings = Settings::getInstance()->get();

        // Check that the Ngnix Helper plugin exist
        if( ! class_exists('PH_Child') ){
            // Plugin missing or removed, remove the hosting setting to be sure the defaults are restored if the plugin is added again
            if( !empty($hosting_settings) && isset( $hosting_settings[Settings::KEY_PROJECT_HUDDLE_SET] ) ){
                Settings::getInstance()->delete(Settings::KEY_PROJECT_HUDDLE_SET );
            }

            return;
        }

        // Check if the hosting defaults have already been set
        // We don't want to override any settings if we've changed them manually on a site
        if( ! empty($hosting_settings) && isset( $hosting_settings[Settings::KEY_PROJECT_HUDDLE_SET] ) ){
            return;
        }

        update_site_option( 'ph_child_plugin_name', 'Watchdog Studio Support');
        update_site_option( 'ph_child_plugin_description', 'Leave a support request or question directly on your website. We\'ll be notified and reply with any follow-up questions, updates or answers to your query.');
        update_site_option( 'ph_child_plugin_author', 'Watchdog Studio');
        update_site_option( 'ph_child_plugin_link', 'https://watchdogstudio.com');

        $hosting_settings[Settings::KEY_PROJECT_HUDDLE_SET] = true;
        Settings::getInstance()->update( $hosting_settings );

        return;
    }
}
