<?php

namespace korndev\hosting\Plugins;

class WPRocket {
    /** @var WPRocket Static property to hold our singleton instance  */
    static $instance = null;

    /**
     * This is our constructor
     */
    private function __construct() {}

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return WPRocket
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * White label WP Rocket
     */
    function whiteLabel(){
        if( ! defined( 'WP_ROCKET_SLUG' ) ) {
            return;
        }

        $default_name = 'KornDev Optimizer';

        $settings = get_option( WP_ROCKET_SLUG );
        if( ! empty( $settings ) && $default_name !== $settings['wl_plugin_name'] ) {
            $settings['wl_plugin_name'] = $default_name;
            $settings['wl_plugin_slug'] = sanitize_title($default_name);
            $settings['wl_plugin_URI']  = 'https://korndev.com';
            $settings['wl_description'] = array('Site speed booster and optimizer');
            $settings['wl_author']      = 'KornDev';
            $settings['wl_author_URI']  = 'https://korndev.com';

            update_option(WP_ROCKET_SLUG, $settings);
        }
    }
}
