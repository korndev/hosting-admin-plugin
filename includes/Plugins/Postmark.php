<?php

namespace korndev\hosting\Plugins;

use korndev\hosting\Keys;
use korndev\hosting\Properties;
use korndev\hosting\Settings;

class Postmark {
    /** @var Postmark Static property to hold our singleton instance */
    static $instance = null;

    /**
     * This is our constructor
     */
    private function __construct() {
    }

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return Postmark
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }
    /**
     * Initialize the setup so we're ready to go when an email needs to be sent
     */
    public function init(){
        $this->setup();
    }

    /**
     * Setup or get the SMTP details to send with
     *
     * @return array|false
     */
    protected function setup(){
        if( ! $this->isEnabled() ){
            return false;
        }

        $verifyOnly = false;
        if( $smtp = $this->getSMTP() ){
            if( is_numeric($smtp) && (int)$smtp >= time() ){
                // Not time to try again
                return false;
            }

            if( isset($smtp['verified']) ) {

                if( is_numeric($smtp['verified'] ) && (int)$smtp['verified'] >= time()) {
                    // Not time to try again
                    return false;
                }

                if( true === $smtp['verified'] ) {
                    return $smtp;
                }
            }

            $verifyOnly = true;
        }

        // Make sure we're only running the setup once at a time
        $transient = 'watchdog_smtp_setup';
        if( get_transient($transient) ){
            return false;
        }

        set_transient( $transient, 1, HOUR_IN_SECONDS );
        try {
            // Get domain for install
            $url    = get_site_url(null, '/');
            $domain = parse_url($url, PHP_URL_HOST);

            // Send POST request to watchdog with key and domain
            $remoteURL = 'https://watchdogstudio.com/wp-json/watchdog/v1/setup-email/';
            $response = wp_remote_post( $remoteURL,
                                        [
                                            'timeout'     => 60,
                                            'redirection' => 5,
                                            'blocking'    => true,
                                            'httpversion' => '1.0',
                                            'sslverify'   => false,
                                            'headers'     => [],
                                            'body'        => [
                                                'key'    => Properties::getInstance()->getHash(),
                                                'domain' => Keys::getInstance()->encrypt('postmark', $domain),
                                                'verifyOnly' => $verifyOnly
                                            ],
                                        ]
            );

            // if failed, store date to try again
            $responseBody = json_decode(wp_remote_retrieve_body($response), true);

            //TODO: Check response code for 200 vs something else
            // if (200 != wp_remote_retrieve_response_code($response)) {
            if( $verifyOnly ){
                if( 'verified' == $responseBody ) {
                    $smtp['verified'] = true;
                } else {
                    $smtp['verified'] = strtotime('+15 minutes');

                    Settings::getInstance()->update([Settings::KEY_EMAIL_SMTP => $smtp]);
                    return false;
                }
            } elseif (empty($responseBody) || ! isset($responseBody['host'])) {
                // If domain isn't on our cloudflare account, need to set WATCHDOG_DISABLE_EMAIL in wp-config to stop retrying
                $this->resetSMTP( strtotime('+1 day') );
                return false;
            } else {
                $smtp = $responseBody;
            }

            // if success, store so we don't try again
            Settings::getInstance()->update([Settings::KEY_EMAIL_SMTP => $smtp]);

            return $smtp;
        } catch( \Throwable $e ){
            //TODO: Log (since email is broken, need to implement slack or another log system
            return false;
        } finally {
            delete_transient( $transient );
        }
    }

    /**
     * Check if site should use Postmark
     *
     * @return bool
     */
    protected function isEnabled(){
        // Check that the define is available
        if( !defined('WATCHDOG_ENABLE_EMAIL_SETUP') ){
            return false;
        }

        // Check that our define is set to TRUE
        if( true !== WATCHDOG_ENABLE_EMAIL_SETUP ){
            return false;
        }

        // Only setup on production environments
        if( ! Properties::getInstance()->isProduction() ){
            return false;
        }

        return true;
    }

    /**
     * Get the SMTP settings
     *
     * @return array|int|false  Array of SMTP settigs, timestamp of next attempt time, false if not setup
     */
    protected function getSMTP(){
        $smtp = Settings::getInstance()->get( Settings::KEY_EMAIL_SMTP );
        if( !empty($smtp) && isset($smtp['provider']) && $smtp['provider'] == 'postmark' ){
            // Already setup
            return $smtp;
        }

        if( is_numeric($smtp) && (int)$smtp > time() ) {
            // Setup attempt failed, return next try time
            return (int)$smtp;
        }

        return false;
    }

    /**
     * Reset the SMTP settings
     *
     * @param int $timestamp    When SMTP settings should start checking again. Default: +1 day
     *
     */
    protected function resetSMTP( $timestamp = null ){
        if( ! $timestamp ){
            $timestamp = strtotime('+1 day');
        }

        // Set SMTP to force check at a future date
        Settings::getInstance()->update( [Settings::KEY_EMAIL_SMTP => $timestamp] );
    }

    /**
     * Setup our SMTP details for emails
     *
     * @param \PHPMailer $mailer
     */
    public function phpMailer( $mailer ){
        if( ! $smtp = $this->setup() ){
            SendGrid::getInstance()->phpMailer( $mailer );
            return;
        }

        // Double check all SMTP settings are available
        if( empty($smtp) || !isset($smtp['host']) || !isset($smtp['port']) || !isset($smtp['user']) || !isset($smtp['auth']) ){
            $this->resetSMTP( strtotime('+1 minute') );
            SendGrid::getInstance()->phpMailer( $mailer );
            return;
        }

        if( ! $user = Keys::getInstance()->decrypt('postmark', $smtp['user']) ){
            $this->resetSMTP( strtotime('+1 minute') );
            SendGrid::getInstance()->phpMailer( $mailer );
            return;
        }

        if( !is_object($mailer) ){
            $mailer = (object)$mailer;
        }

        $mailer->Mailer     = 'smtp';
        $mailer->Host       = $smtp['host'];
        $mailer->Port       = $smtp['port'];
        $mailer->Username   = $user;
        $mailer->Password   = $user;
        $mailer->SMTPAuth   = true;
        $mailer->SMTPSecure = $smtp['auth'];
        $mailer->addCustomHeader('X-PM-Tag', Properties::getInstance()->get(Properties::KEY_DEFAULT_SENDING_DOMAIN) );

        // Always make sure emails are sending with the host domain
        Properties::getInstance()->setPHPMailer( $mailer );
        add_action('wp_mail_failed', [$this, 'mailFailed']);
    }

    /**
     * Catch SMTP errors and resend email via backup method
     *
     * @param \WP_Error $error
     */
    public function mailFailed( $error ){
        // Check if email is disabled
        if( get_transient('watchdog_disable_email_setup') ){
            return;
        }

        foreach( $error->get_error_messages() as $id => $message ){
            if( false !== strpos($message, 'SMTP Error') ){
                //TODO: phone home to send slack message ?

                $this->resetSMTP( strtotime('+1 minute') );

                //TODO: Review if we really want to sent the email - at this point, wp_mail is already returning false
                
//                // Get the email details
//                $mail = $error->get_error_data($id);
//
//                // Attempt to send email again (should go through SendGrid)
//                wp_mail( $mail['to'], $mail['subject'], $mail['message'], $mail['headers'], $mail['attachments'] );
                break;
            }
        }
    }
}