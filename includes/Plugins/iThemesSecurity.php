<?php

namespace korndev\hosting\Plugins;

use korndev\hosting\Properties;
use korndev\hosting\Settings;

class iThemesSecurity {
    /** @var iThemesSecurity Static property to hold our singleton instance  */
    static $instance = null;

    protected $isWPE = false;

    /**
     * This is our constructor
     */
    private function __construct() {
        $Properties = Properties::getInstance();
        $this->isWPE = $Properties->isHostWPE();
    }

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return iThemesSecurity
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Set default settings
     *
     * @return void
     */
    public function setDefaults(){
        $hosting_settings = Settings::getInstance()->get('', is_multisite() );

        // Check that the iThemes Security plugin exist
        if( ! Properties::getInstance()->isiThemesInstalled() ){
            // Plugin missing or removed, remove the hosting setting to be sure the defaults are restored if the plugin is added again
            if( ! empty($hosting_settings) && isset( $hosting_settings[Settings::KEY_SECURITY_SET] ) ){
                Settings::getInstance()->delete( Settings::KEY_SECURITY_SET, is_multisite() );
            }

            return;
        }

        // Check if the hosting defaults have already been set
        // We don't want to override any settings if we've changed them manually on a site
        $settings_set = false;
        if( ! empty($hosting_settings)
            && isset( $hosting_settings[Settings::KEY_SECURITY_SET] )
            // Since v2.2.0 -- force default settings if hide_admin_bar is not set to 1
            && 1 == \ITSEC_Modules::get_setting( 'global', 'hide_admin_bar' )
        ){
            $settings_set = true;
        }

        // Get the support user
        $korndev_support = get_user_by('login', Properties::getInstance()->get(Properties::KEY_SUPPORT_USER) );

        // Settings we ALWAYS set
        // Check if the default notification settings are set
        if( $korndev_support->user_email !== \ITSEC_Modules::get_setting('notification-center', 'from_email') ) {
            \ITSEC_Modules::set_setting('notification-center', 'from_email', $korndev_support->user_email );
            \ITSEC_Modules::set_setting('notification-center', 'default_recipients', [ 'user_list' => [0 => (string)$korndev_support->ID] ] );
        }

        if( $settings_set ){
            return;
        }

        //TODO: Add required keys: digest_messages, email_contacts, automatic_update_emails
        $default_settings = array(
            'global'              => array(
                'write_files'               => true,
                'lockout_message'           => 'Your computer has been locked out due to too many invalid login attempts.',
                'user_lockout_message'      => 'You have been locked out due to too many invalid login attempts.',
                'community_lockout_message' => 'Your IP address has been flagged as a threat by our security system.',
                'blacklist'                 => true,
                'blacklist_count'           => 5,
                'blacklist_period'          => 7,   // days
                'lockout_period'            => 15,  // in minutes
                'lockout_white_list'        => array(
                    // Justin Korn home
                    0 => '69.167.26.136',
                    // ManageWP
                    1 => '34.211.180.66',
                    2 => '54.70.65.107',
                    3    => '34.210.224.7',
                    4    => '52.41.5.108',
                    5    => '52.35.72.129',
                    6    => '54.191.137.17',
                    7    => '35.162.254.253',
                    8    => '52.11.12.231',
                    9    => '52.11.29.70',
                    10    => '52.11.54.161',
                    11    => '52.24.142.159',
                    12   => '52.25.191.255',
                    13    => '52.27.181.126',
                    14    => '52.34.126.117',
                    15    => '52.34.254.47',
                    16    => '52.35.82.99',
                    17    => '52.36.28.80',
                    18    => '52.38.106.97',
                    19    => '52.39.177.152',
                    20    => '52.41.230.148',
                    21    => '52.41.237.12',
                    22    => '52.42.126.166',
                    23    => '52.43.13.71',
                    24    => '52.43.76.224',
                    25    => '52.88.96.110',
                    26    => '52.89.155.51',
                    27    => '54.148.73.118',
                    28    => '54.186.37.105',
                    29    => '54.187.92.57',
                    30    => '54.191.32.65',
                    31    => '54.191.67.23',
                    32    => '54.191.80.119',
                    33    => '54.191.135.209',
                    34    => '54.191.136.176',
                    35    => '54.191.148.85',
                    36    => '54.191.149.8',
                    37    => '52.26.122.21',
                    38    => '52.24.187.29',
                    39    => '52.89.85.107',
                    40    => '54.186.128.167',
                    41    => '54.191.40.136',
                    42    => '52.24.62.11',
                    43    => '52.88.119.122',
                    44    => '54.191.148.225',
                    45    => '54.191.151.18',
                    46    => '52.89.94.121',
                    47    => '52.25.116.116',
                    48    => '52.88.215.225',
                    49    => '54.186.143.184',
                    50    => '52.88.197.180',
                    51    => '52.27.171.126',
                    // Sucuri Scan
                    52    => '66.228.40.185',
                    53    => '50.116.36.93',
                    54    => '198.58.96.212'
                ),
                'log_type'                  => 'database',
                'log_rotation'              => 7,   // Days
                'allow_tracking'            => false,
                'proxy'                     => 'automatic',
                'hide_admin_bar'            => true,
                'show_error_codes'          => false,
                'enable_grade_report'       => false,
                'show_new_dashboard_notice' => false,
                'show_security_check'       => false,
                'notification_email'        => array(0 => $korndev_support->user_email),
                'backup_email'              => array(0 => $korndev_support->user_email),
                'digest_last_sent'          => null
            ),
            'network-brute-force' => array(
                'api_key'       => '',      // Need to manually set on each install
                'api_secret'    => '',
                'enable_ban'    => true,
                'updates_optin' => false,
                'api_nag'       => false,
                'email'         => $korndev_support->user_email,
            ),
            '404-detection'       => array(
                'check_period'    => 5,
                'error_threshold' => 20,
                'white_list'      => array(
                    0 => '/favicon.ico',
                    1 => '/robots.txt',
                    2 => '/apple-touch-icon.png',
                    3 => '/apple-touch-icon-precomposed.png',
                    4 => '/wp-content/cache',
                    5 => '/browserconfig.xml',
                    6 => '/crossdomain.xml',
                    7 => '/labels.rdf',
                    8 => '/trafficbasedsspsitemap.xml',
                    9 => '/wp-content/wp-rocket-config',
                ),
                'types'           => array(
                    0 => '.jpg',
                    1 => '.jpeg',
                    2 => '.png',
                    3 => '.gif',
                    4 => '.css',
                ),
            ),
            'ban-users'           => array(
                'default'          => true,
                'enable_ban_lists' => true,
                'host_list'        => array(),
                'agent_list'       => array(),
            ),
            'brute-force'         => array(
                'max_attempts_host' => 5,
                'max_attempts_user' => 10,
                'check_period'      => 5,
                'auto_ban_admin'    => true,
            ),
            'file-change'         => array(
                'method'         => 'exclude',
                'file_list'      => array(
                    0  => 'wp-content/plugins/hyperdb-1-1/',
                    1  => 'wp-content/plugins/hyperdb/',
                    2  => 'wp-content/mu-plugins/',
                    3  => 'wp-content/wp-rocket-config/',
                    4 => 'wp-content/uploads/',
                    5  => 'wp-content/upgrade/',
                    6  => 'wp-content/cache/',
                    7  => 'wp-content/debug.log',
                    8  => 'wp-content/advanced-cache.php',
                    9 => 'wp-content/object-cache.php',
                    10 => 'cache/',
                    11  => 'sitemap.xml.gz',
                    12  => 'sitemap.xml',
                    13 => 'sucuri_listcleaned.php',
                    14 => 'sucuri-version-check.php',
                    15 => 'sucuri-db-cleanup.php',
                    16 => 'sucuri-d8368fbae2b4ff851facbbc57dfc8839.php',
                    17 => 'sucuri-cleanup.php',
                    18 => '.sucuriquarantine/',
                    19 => '.gitattributes',
                    20 => '.gitignore',
                    21 => '.git/',
                    22 => '_wpeprivate/',
                ),
                'types'          => array(
                    0 => '.jpg',
                    1 => '.jpeg',
                    2 => '.png',
                    3 => '.gif',
                    4 => '.log',
                    5 => '.mo',
                    6 => '.po',
                    7 => '.ico',
                    8 => '.sql',
                ),
                'split'          => false,
                'notify_admin'   => false,
                'last_chunk'     => false,
                'show_warning'   => false,
                'email'          => null,
            ),
//            'online-files'        => array(
//                'compare_file_hashes' => true,
//            ),
            'password-requirements'    => array(
                'enabled_requirements' => array(
                    'strength' => ($this->isWPE) ? false : true, // Enable Strong passwords
                    'age'   => false,   // Disable password expiration
                    'hibp'  => true     // Refuse Compromised Passwords via Have I Been Pawned
                ),
                'requirement_settings' => array(
                    'strength' => array(
                        'role' => 'author'
                    ),
                    'age' => array(
                        'role' => 'author'
                    ),
                    'hibp' => array(
                        'role' => 'subscriber'
                    )
                )
            ),
            'wordpress-tweaks'    => array(
                'wlwmanifest_header'          => true,
                'edituri_header'              => true,
                'comment_spam'                => true,
                'file_editor'                 => false,
                'disable_xmlrpc'              => 1, // 1 = Disable Pingbacks
                'allow_xmlrpc_multiauth'      => ($this->isWPE) ? true : false,
                'rest_api'                    => ($this->isWPE) ? 'default-access' : 'restrict-access',
                'login_errors'                => false,
                'force_unique_nicename'       => false,
                'disable_unused_author_pages' => true,
                'block_tabnapping'            => true,
                'valid_user_login_type'       => 'both',
                'patch_thumb_file_traversal'  => true,
            ),
            'recaptcha'           => array(     // Need to setup API manually for each install
                'type'            => 'invisible',
                'v3_include_location' => 'required',
                'login'           => true,
                'register'        => true,
                'comments'        => true,
                'language'        => '',
                'error_threshold' => 7,
                'check_period'    => 5,

                'theme'           => false,
                'validated'       => false,
                'last_error'      => '',
            ),
            'two-factor'          => array(
                'available_methods'        => 'all',
                'protect_user_type'        => 'disabled',
                'protect_user_type_roles'  => array(
                    0 => 'administrator',
                    1 => 'editor',
                    2 => 'author',
                ),
                'exclude_type' => 'disabled',
                'protect_vulnerable_users' => false,
                'protect_vulnerable_site'  => false,
                'disable_first_login' => true,
                'on_board_welcome' => "Two-factor authentication setup is optional, but recommended for additional security by adding an important extra layer of protection to your login by combining something you know, your password, with something you have, your Phone or Email, preventing attackers from gaining access to your account even if you lose control of your password.\n\nWhen you login using Two-factor authentication you’ll be prompted to enter a secondary Authentication Code from your Phone or Email.",
                'application_passwords_type' => 'enabled'
            ),
            'user-logging'        => array(
                'role' => 'editor',
            ),
            'version-management'  => array(
                'wordpress_automatic_updates'  => false,
                'plugin_automatic_updates'     => 'none',
                'theme_automatic_updates'      => 'none',
                'strengthen_when_outdated'     => false,
                'scan_for_old_wordpress_sites' => false,
            ),
            'system-tweaks'       => array(
                'protect_files'            => true,
                'directory_browsing'       => true,
                'request_methods'          => ($this->isWPE) ? false : true,
                'suspicious_query_strings' => ($this->isWPE) ? false : true,
                'non_english_characters'   => true,
                'long_url_strings'         => true,
                'write_permissions'        => false,
                'uploads_php'              => true,
                'plugins_php'              => false,
                'themes_php'               => false,
            ),
            'notification-center' => array(
                'from_email' =>  $korndev_support->user_email,
                'default_recipients' => array(
                    'user_list' => array(
                        0 => (string)$korndev_support->ID,
                    ),
                ),
                'notifications' => array(
                    'automatic-updates-debug' => array(
                        'enabled' => false,
                        'recipient_type' => 'default',
                        'user_list' => array(
                            0 => (string)$korndev_support->ID,
                        )
                    ),
                    'file-change'           => array(
                        'enabled'   => false,
                        'subject' => 'File Change Warning',
                        'recipient_type' => 'default',
                        'user_list' => array(
                            0 => (string)$korndev_support->ID,
                        )
                    ),
                    'grade-report-change' => array(
                        'enabled' => false,
                        'subject' => 'Website Security Grade has Changed',
                        'recipient_type' => 'default',
                        'user_list' => array(
                            0 => (string)$korndev_support->ID,
                        )
                    ),
                    'inactive-users'        => array(
                        'enabled'   => false,
                        'subject' => 'Inactive Users',
                        'schedule'  => 'weekly',     // daily, monthly
                        'recipient_type' => 'default',
                        'user_list' => array(
                            0 => (string)$korndev_support->ID,
                        )
                    ),
                    'magic-link-login-page' => array(
                        'subject' => 'Login Link',
                        'message' => 'Hello {{ $display_name }},' . "\n\n" . 'For security purposes, please click the button below to login to {{ $site_title }}. {{ $login_url }}',
                    ),
                    'digest'                => array(
                        'enabled'   => false,
                        'subject' => 'Security Digest',
                        'schedule'  => 'weekly',
                        'recipient_type' => 'default',
                        'user_list' => array(
                            0 => (string)$korndev_support->ID,
                        )
                    ),
                    'import-export' => array(
                        'subject' => 'Security Settings Export',
                        'message' => 'Attached is the settings file for {{ site_title }} at {{ $site_url }} created on {{ $date }} at {{ $time }}.'
                    ),
                    'lockout'               => array(
                        'enabled'   => false,
                        'subject' => 'Site Lockout Notification',
                        'recipient_type' => 'default',
                        'user_list' => array(
                            0 => (string)$korndev_support->ID,
                        )
                    ),
                    'two-factor-email'      => array(
                        'subject' => 'Login Authentication Code',
                        'message' => 'Hello {{ $display_name }},' . "\n\n" . 'Complete logging into your website ({{ $site_title }}) as {{ $username }} with the verification code below.',
                    ),
                    'two-factor-confirm-email' => array(
                        'subject' => 'Login Authentication Code',
                        'message' => 'Hello {{ $display_name }},' . "\n\n" . 'Complete logging into your website ({{ $site_title }}) as {{ $username }} with the verification code below.',
                    ),
                    'two-factor-reminder'   => array(
                        'subject' => 'Please setup Two Factor Authentication',
                        'message' => 'Hello {{ $display_name }},' . "\n\n" . '{{ $requester_display_name }} from {{ $site_title }} has asked that you set up Two Factor Authentication.',
                    ),
                    'backup' => array(
                        'subject' => 'Database Backup',
                        'recipient_type' => 'default',
                        'email_list' => array(
                            0 => $korndev_support->user_email,
                        ),
                    ),
                    'malware-scheduling' => array(
                        'enabled'   => false,
                        'recipient_type' => 'default',
                        'user_list' => array(
                            0 => (string)$korndev_support->ID,
                        )
                    )
                ),
                'admin_emails'  => array(
                    0 => $korndev_support->user_email,
                ),
            ),
        );
        \ITSEC_Storage::set_all( array_merge( \ITSEC_Storage::get_all(), $default_settings ) );

        // Enable on all installs
        $default_modules = [
            'wordpress-tweaks',
            'system-tweaks',
            'ban-users',
            'magic-links',
            'two-factor',
            'user-logging',
            'file-change',
            '404-detection'
        ];

        // Enable only when site is not running on WPE
        if( ! $this->isWPE ){
            $add_modules = [
                'network-brute-force',
                'brute-force',
                'strong-passwords'
            ];

            $default_modules = array_merge( $default_modules, $add_modules );
        }

        \ITSEC_Modules::set_active_modules( $default_modules );
        \ITSEC_Modules::deactivate('backup');
        \ITSEC_Modules::deactivate('malware-scheduling');
        \ITSEC_Modules::deactivate('version-management');

        if( $this->isWPE ){
            \ITSEC_Modules::deactivate('network-brute-force');
            \ITSEC_Modules::deactivate('brute-force');
        }

        $hosting_settings[Settings::KEY_SECURITY_SET] = true;
        Settings::getInstance()->update( $hosting_settings, is_multisite() );
    }

}
