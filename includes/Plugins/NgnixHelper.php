<?php
namespace korndev\hosting\Plugins;

use korndev\hosting\Settings;

class NgnixHelper {

    CONST OPTIONS = 'rt_wp_nginx_helper_options';

    /** @var NgnixHelper Static property to hold our singleton instance  */
    static $instance = null;

    /**
     * This is our constructor
     */
    private function __construct() {}

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return NgnixHelper
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Set default settings
     *
     * @return void
     */
    function setDefaults(){
        if( is_multisite() ){
            return;
        }

        $hosting_settings = Settings::getInstance()->get();

        // Check that the Ngnix Helper plugin exist
        if( ! class_exists('Nginx_Helper') ){
            // Plugin missing or removed, remove the hosting setting to be sure the defaults are restored if the plugin is added again
            if( !empty($hosting_settings) && isset( $hosting_settings[Settings::KEY_NGNIX_HELPER_SET] ) ){
                Settings::getInstance()->delete(Settings::KEY_NGNIX_HELPER_SET );
            }

            return;
        }

        // Check if the hosting defaults have already been set
        // We don't want to override any settings if we've changed them manually on a site
        if( ! empty($hosting_settings) && isset( $hosting_settings[Settings::KEY_NGNIX_HELPER_SET] ) ){
            return;
        }

        $settings = [
            'enable_purge'                     => 1,
            'cache_method'                     => 'enable_redis',
            'redis_hostname'                   => '127.0.0.1',
            'redis_port'                       => '6379',
            'redis_prefix'                     => 'nginx-cache:',
            'purge_homepage_on_edit'           => 1,
            'purge_homepage_on_del'            => 1,
            'purge_archive_on_edit'            => 1,
            'purge_archive_on_del'             => 1,
            'purge_archive_on_new_comment'     => 0,
            'purge_archive_on_deleted_comment' => 0,
            'purge_page_on_mod'                => 1,
            'purge_page_on_new_comment'        => 1,
            'purge_page_on_deleted_comment'    => 1
        ];

        if( update_site_option( self::OPTIONS, wp_parse_args( $settings, get_site_option( self::OPTIONS ) ) ) ){
            $hosting_settings[Settings::KEY_NGNIX_HELPER_SET] = true;
            Settings::getInstance()->update( $hosting_settings );
        }

        return;
    }
}
