<?php

namespace korndev\hosting\Plugins;

use korndev\hosting\Settings;

class Imagify {
    /** @var Imagify Static property to hold our singleton instance  */
    static $instance = null;

    /**
     * This is our constructor
     */
    private function __construct() {}

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return Imagify
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function init(){
        if( $this->isActive() ){
            add_filter( 'manage_media_columns', [$this, 'renameImagifyMediaColumn'], 999 );
        }
    }

    protected function isActive() {
        if( class_exists('Imagify_Options') ){
            return true;
        }
        return false;
    }

    protected function getOptionName(){
        return \Imagify_Options::get_instance()->get_option_name();
    }

    /**
     * Rename the Media column that Imagify sets
     *
     * @param array $columns
     *
     * @return array
     */
    public function renameImagifyMediaColumn( $columns ){
        $columns['imagify_optimized_file'] = __('Optimize', 'korndevhost');
        return $columns;
    }

    /**
     * Set default settings
     *
     * @return void
     */
    public function setDefaults(){
        $hosting_settings = Settings::getInstance()->get();

        // Check that the Imagify plugin exist
        if( ! $this->isActive() ){
            // Plugin missing or removed, remove the hosting setting to be sure the defaults are restored if the plugin is added again
            if( !empty($hosting_settings) && isset( $hosting_settings[Settings::KEY_IMAGIFY_SET] ) ){
                Settings::getInstance()->delete(Settings::KEY_IMAGIFY_SET );
            }
            return;
        }

        // Check if the hosting defaults have already been set
        // We don't want to override any settings if we've changed them manually on a site
        if( ! empty($hosting_settings) && isset( $hosting_settings[Settings::KEY_IMAGIFY_SET] ) ){
            return;
        }

        $imagify_settings = get_option( $this->getOptionName() );
        if( empty( $imagify_settings ) ) {
            return;
        }

        $imagify_settings['optimization_level'] = 2; // 1 = Aggressive; 2 = Ultra
        $imagify_settings['auto_optimize'] = 1;
        $imagify_settings['backup'] = 1;

        //TODO: Get working
        if( function_exists( 'get_imagify_max_intermediate_image_size') ){
            $max_sizes = get_imagify_max_intermediate_image_size();
            $imagify_settings['resize_larger'] = 1;
            $imagify_settings['resize_larger_w'] = (string)$max_sizes['width'];
        }

        $imagify_settings['admin_bar_menu'] = 0;
        $imagify_settings['partner_links'] = 0;

        if( update_option( $this->getOptionName(), $imagify_settings ) ){
            $hosting_settings[Settings::KEY_IMAGIFY_SET] = true;
            Settings::getInstance()->update( $hosting_settings );
        }

        return;
    }
}
