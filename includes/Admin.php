<?php

namespace korndev\hosting;

use korndev\hosting\Plugins\SendGrid;

class Admin {
    /** @var Admin Static property to hold our singleton instance  */
    static $instance = null;

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return Admin
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * WP Admin Initialization
     */
    public function adminInit(){
        Permissions::getInstance()->setupDefaultAdmins();

        wp_enqueue_style( 'korndev-hosting-css', Plugin::getInstance()->getURL('assets/css/style.css'), false, Versions::getInstance()->getCurrent() );

        // Dashboard Support Form
        wp_enqueue_script( 'korndev-hosting-supportform', Plugin::getInstance()->getURL('assets/js/form_send.js'), array('jquery'), Versions::getInstance()->getCurrent(), true );
        wp_localize_script(
            'korndev-hosting-supportform',
            'KornDevHostingSupport',
            array(
                'ajax_support_nonce' => wp_create_nonce( 'korndev-hosting-support-nonce' ), // Generate WP Nonce for security
            )
        );

        // Dashboard Purge Cache action
        wp_enqueue_script( 'korndev-hosting-purgecache', Plugin::getInstance()->getURL('assets/js/purge_cache.js'), array('jquery'), Versions::getInstance()->getCurrent(), true );
        wp_localize_script(
            'korndev-hosting-purgecache',
            'KornDevHostingCache',
            array(
                'ajax_cache_nonce' => wp_create_nonce( 'korndev-hosting-cache-nonce' ), // Generate WP Nonce for security
            )
        );

        //TODO: Move to Imagify Class ?

        // Remove Imagify rating notice
        if( ! get_site_transient('imagify_seen_rating_notice') ){
            set_site_transient( 'imagify_seen_rating_notice', true );
        }
        // Stop rating cron from being set and running
        if( ! get_site_transient( 'do_imagify_rating_cron' ) ){
            set_site_transient( 'do_imagify_rating_cron', 'no' );
            wp_clear_scheduled_hook( 'imagify_rating_event' );
        }
        // Don't show grid view warning
        if( ! Permissions::getInstance()->isSuperUser( true ) && method_exists( 'Imagify_Notices', 'dismiss_notice' ) ) {
            \Imagify_Notices::dismiss_notice( 'grid-view' );
        }
    }

    /**
     * Custom WordPress Footer
     */
    public function footer( $admin_footer_text ) {
        //TODO: Possibly build url differently for multi-site with sub-folders vs directories

        $text = '<a href="' . Properties::getInstance()->get(Properties::KEY_HOSTING_URL) . '" target="_blank" rel="noopener noreferrer"><img src="' . Plugin::getInstance()->getURL('assets/imgs/watchdog-small-icon.png') . '" alt="' . Properties::getInstance()->get(Properties::KEY_NAME) . '" style="height:25px;" /></a> WordPress Hosting by <a href="' . Properties::getInstance()->get(Properties::KEY_HOSTING_URL) . '" target="_blank" rel="noopener noreferrer">' . Properties::getInstance()->get(Properties::KEY_NAME) . '</a>';

        return '<span id="footer-korndev-hosting">' . $text . '</span>';
    }

    /**
     * Customize the Dashboard widgets
     */
    public function dashboardWidgets(){
        wp_add_dashboard_widget('korndev_hosting_info', 'Hosting Information', [$this, 'dashboardWidget'] );

        // Globalize the metaboxes array, this holds all the widgets for wp-admin
        global $wp_meta_boxes;

        // Grab the new widget
        $hosting_info_widget = $wp_meta_boxes['dashboard']['normal']['core']['korndev_hosting_info'];

        // Remove the new widget from 'normal'
        unset($wp_meta_boxes['dashboard']['normal']['core']['korndev_hosting_info']);

        // Add new widget to 'side'
        $wp_meta_boxes['dashboard']['side']['core']['korndev_hosting_info'] = $hosting_info_widget;

        // Grab side widget area
        $side_dashboard = $wp_meta_boxes['dashboard']['side']['core'];

        // Backup and delete our new dashboard widget from the end of the array
        $korndev_hosting_info = array('korndev_hosting_info' => $side_dashboard['korndev_hosting_info']);
        unset($side_dashboard['korndev_hosting_info']);

        // Merge the two arrays together so our widget is at the beginning
        $sorted_dashboard = array_merge($korndev_hosting_info, $side_dashboard);

        // Save the sorted array back into the original metaboxes
        $wp_meta_boxes['dashboard']['side']['core'] = $sorted_dashboard;

        remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
        remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
        remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
        remove_meta_box('dashboard_primary', 'dashboard', 'side');
        remove_meta_box('dashboard_secondary', 'dashboard', 'side');

        if( ! Permissions::getInstance()->isSuperUser(true) ){
            remove_meta_box('itsec-dashboard-widget', 'dashboard', 'normal');
        }

        if( isset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']) ) {
            $dashboard_incoming_links = $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'];
            unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
            $wp_meta_boxes['dashboard']['side']['core']['dashboard_incoming_links'] = $dashboard_incoming_links;
        }
    }

    /**
     * Add a widget in WordPress Dashboard
     *
     * @since 1.0
     * @since 1.4 Added CSV parsing and visitor count
     * @since 1.4.4 Added plugin version
     * @since 1.5.9 Added support url
     * @since 1.8.0 Add multisite check
     * @since 1.11.0 Add support form
     * @since 1.12.0 Add transient for visitor count
     * @since 1.12.3 Restrict support form to Administrators
     * @since 1.12.8 Skip WP Engine stats if not running on WP Engine
     */
    public function dashboardWidget() {
        $current_user = wp_get_current_user();

        echo '<ul>
	<li>Hosting Provider: <a href="' . Properties::getInstance()->get(Properties::KEY_HOSTING_URL) . '" target="_blank">' . Properties::getInstance()->get(Properties::KEY_NAME) . '</a></li>';
        if ( Permissions::getInstance()->getDefaultSuperUsers($current_user->user_login) ):
            echo '<li>Admin Plugin Version: ' . Versions::getInstance()->getCurrent() . '</li>';
        endif;

        if( defined( 'WPE_APIKEY') && defined( 'PWP_NAME' ) && ! is_multisite() ) {
            $csv_url = 'https://api.wpengine.com/1.2/index.php?account=' . PWP_NAME . '&account_name=' . PWP_NAME . '&wpe_apikey=' . WPE_APIKEY . '&method=weblogs-daily-summary';

            $visitors = get_transient('korndev_hosting_visitor_count');
            if ($visitors === false) {
                $csv = wp_remote_get($csv_url);
                if (is_wp_error($csv)) {
                    $visitors = -1;
                } else {
                    $csv_rows  = str_getcsv($csv['body'], "\n"); //parse the rows
                    $row_count = 0;
                    $visitors  = 0;
                    foreach ($csv_rows as $row) {
                        if ($row_count !== 0):
                            $data = explode(',', $row);
                            // [0] = tstamp,
                            // [1] = customer_id,
                            // [2] = cluster,
                            // [3] = visitors,
                            // [4] = objects_served_directly,
                            // [5] = objects_served_cdn,
                            // [6] = gb_served_directly,
                            // [7] = gb_served_cdn,
                            // [8] = gb_total,
                            // [9] = uncached_dynamic_hits,
                            // [10] = backend_server_processing_hours
                            $data_date  = strtotime($data[0]);
                            $last_month = strtotime("-31 days");
                            if ($data_date >= $last_month):
                                $visitors += $data[3];
                            endif;
                        endif;
                        $row_count++;
                    }
                }

                set_transient('korndev_hosting_visitor_count', $visitors, 60 * 60 * 24); // 24 hours
            }

            if ($visitors < 0) {
                echo '<li>31 Day Visit Count: <span style="font-weight:bold;">Currently Unavailable</span></li>';
            } else {
                echo '<li>31 Day Visit Count: <span style="font-weight:bold;">' . number_format($visitors) . '</span> (<a href="' . $csv_url . '">Download Stats</a>)</li>';
            }
        }

        // Display the button to clear cache
        if( class_exists('Nginx_Helper') && current_user_can('publish_pages') ){ ?>
            <button id="korndev-clearCache" class="button button-primary">Purge Cache</button>
            <img class="korndev-processing" src="<?php echo Plugin::getInstance()->getURL('assets/imgs/sending.gif'); ?>" alt="Processing Request" style="display: none; margin: 10px 20px 0 0">
            <div class="korndev-cache-feedback"><p></p></div>
        <?php }

        if( !is_multisite() && current_user_can( 'publish_pages' ) ){
            $user = wp_get_current_user();
            ?>
            <li>
                <form id="korndev-support" name="korndev-support" action="<?php echo admin_url(); ?>" method="post" class="initial-form hide-if-no-js">
                    <div class="input-text-wrap">
                        <h4>Contact Hosting Support</h4>
                        <p>Use the form below to contact your hosting support team.</p>
                        </p>
                    </div>

                    <div class="input-text-wrap">
                        <label for="mail_title">Subject</label>
                        <input name="mail_title" id="korndev_title" autocomplete="off" type="text" />
                    </div>

                    <div class="textarea-wrap">
                        <label for="mail_content">Message</label>
                        <p class="description">Please be as specific as you can, providing links and examples for us to reproduce the issues</p>
                        <textarea name="mail_content" id="korndev_content" class="mceEditor" rows="3" cols="15" autocomplete="off" value=""></textarea>
                    </div>

                    <div class="submit" style="margin-top:12px">
                        <input name="install" id="korndev_install" autocomplete="off" type="hidden" value ="<?php echo esc_url(get_site_url()); ?>" />
                        <input name="author_name" id="korndev_author" autocomplete="off" type="hidden" value ="<?php echo esc_attr($user->display_name); ?>" />
                        <input name="author_email" id="korndev_author_email" class="author" type="hidden" value="<?php echo $user->user_email; ?>" />
                        <input name="save" id="send-message" class="button button-primary" value="Send Message" type="submit" />
                        <img class="korndev-sending" src="<?php echo Plugin::getInstance()->getURL('assets/imgs/sending.gif'); ?>" alt="Sending Message" style="display: none; margin: 10px 20px 0 0">
                        <br class="clear" />
                    </div>
                    <div class="korndev-support-feedback">
                        <p>Alternative Support Options:
<!--                            <br />-->
<!--                            <a href="--><?php //echo Properties::getInstance()->get(Properties::KEY_SUPPORT_URL); ?><!--" target="_blank" rel="noopener noreferrer">Web Form</a>-->
                            <br />
                            Email: <a href="mailto:<?php echo Properties::getInstance()->get(Properties::KEY_SUPPORT_EMAIL); ?>" target="_blank" rel="noopener noreferrer"><?php echo Properties::getInstance()->get(Properties::KEY_SUPPORT_EMAIL); ?></a>
                        </p>
                    </div>
                </form>
            </li>
            <?php
        }

        echo '</ul>';

        /*
          <div class="rss-widget"><h4>Status Report:</h4>';
          wp_widget_rss_output(array(
          'url' => 'http://status.wpengine.com/feed/',
          'title' => 'Hosting Status',
          'items' => 3,
          'show_summary' => 1,
          'show_author' => 0,
          'show_date' => 1
          ));
          echo '</div>';
         */
    }

    /**
     * Filter Admin Bar
     */
    public function filterAdminBar(){
        global $wp_admin_bar;

        if ( ! Permissions::getInstance()->isSuperUser( true ) ) {
            // Since v 1.12.5
            $wp_admin_bar->remove_menu('imagify');  // Remove Imagify if enabled
            // Since v 1.13.0
//            $wp_admin_bar->remove_node('rocket-settings');    // Remove WP-Rocket settings menu
        }

        if ( ! Permissions::getInstance()->isSuperUser() ) {
            if( Properties::PROVIDER_WPENGINE == Properties::getInstance()->get(Properties::KEY_PROVIDER) ) {
                $wp_admin_bar->remove_menu('wpengine_adminbar', 20);    // Remove WPE
            }

            $wp_admin_bar->remove_menu('updates');	// Remove the updates link
        }
    }

    /**
     * Remove Menus
     */
    public function removeMenus(){
        if( ! Permissions::getInstance()->isSuperUser( true ) ):
            // iThemes Security
            remove_menu_page('itsec'); // Remove iThemes Security
            remove_submenu_page('options-general.php', 'ithemes-licensing');

            // Imagify
            remove_submenu_page('options-general.php', 'imagify');
            remove_submenu_page('upload.php', 'imagify-bulk-optimization');

            // WPMerge
            remove_menu_page( 'wpmerge_prod_setting' );
            remove_submenu_page( 'wpmerge_dev_options', 'wpmerge_dev_settings' );

            // Cleantalk
            remove_submenu_page( 'options-general.php', 'cleantalk');
            remove_submenu_page( 'edit-comments.php', 'ct_check_spam');

            // WP fail2ban
            remove_menu_page( 'wp-fail2ban' );

            // Project Huddle / Feedback
            remove_submenu_page( 'options-general.php', 'feedback-connection-options' );
        endif;

        if ( ! Permissions::getInstance()->isSuperUser() ):
            if( Properties::getInstance()->isHostWPE() ) {
                remove_menu_page('wpengine-common'); // Remove WP Engine Admin Menu
            }

            // Since v 1.12.0
            remove_submenu_page( 'index.php', 'update-core.php' );
            remove_submenu_page( 'themes.php', 'theme-editor.php' );
            remove_submenu_page( 'plugins.php', 'plugin-editor.php' );

            // Remove Migrate DB Pro
            remove_submenu_page( 'tools.php', 'wp-migrate-db-pro' );
        endif;
    }

    /**
     * Show/Hide Must Use (MU) plugins
     */
    public function showMustUse($bool = true, $type = 'mustuse') {
        if ( Permissions::getInstance()->isSuperUser() ) {
            return true;
        }

        return false;
    }

    /**
     * Function to filter out users from the user list
     *
     * @param $user_search
     *
     * @return mixed
     */
    public function filterUserList($user_search) {
        global $wpdb;
        $filter_user = 'wpengine';

        if ( Permissions::getInstance()->isSuperUser( true ) ) {
            return $user_search;
        } else {
            global $wpdb;
            $user_search->query_where =
                str_replace('WHERE 1=1', "WHERE 1=1 AND {$wpdb->users}.user_login <> '$filter_user'", $user_search->query_where
                );
        }
    }


    /**
     * Function to filter out plugins from the plugin admin page
     *
     * @param array $plugins
     *
     * @return array
     */
    public function filterPluginList( $plugins ) {
        $hide_plugins = array(
            'KornDev Hosting Administrator',
            'GoDaddy Pro Sites Worker',
            'ManageWP - Worker',
            'KornDev Worker',
            'KornDev Website Care',
            'iThemes Security Pro',
            // Since 2.4.0
            'Redis Object Cache',
            'Nginx Helper',
            'KornDev Security',
            'iThemes Security',
            'WPMerge',
            'WP fail2ban',
            'Anti-Spam by CleanTalk',
        );

        if ( Permissions::getInstance()->isSuperUser( true ) ) {
            return $plugins;
        } else {
            $viewable_plugins = array();

            $keys = array_keys($plugins);
            $num = 0;
            foreach ($plugins as $plugin) {
                if (array_search($plugin['Name'], $hide_plugins) === false) {
                    $viewable_plugins[$keys[$num]] = $plugin;
                }
                $num++;
            }
            return $viewable_plugins;
        }
    }

    /**
     * Hook into the Ngnix Cache plugin and purge the cache
     */
    public function purgeCache(){
        $nonce = $_POST['cache_nonce'];
        if( !wp_verify_nonce( $nonce, 'korndev-hosting-cache-nonce' ) ):
            $response = array('success' => false, 'message' => 'There seems to be an issue with the security of your request.');
            wp_send_json( $response );
        endif;

        do_action('rt_nginx_helper_purge_all');

        $response = array('success' => true, 'message' => 'Purge of Cache has been initiated.');
        wp_send_json($response);
    }

    /**
     * Send details from support form filled out within the dashboard
     */
    public function sendSupportMessage() {
        $nonce = $_POST['support_nonce'];
        if( !wp_verify_nonce( $nonce, 'korndev-hosting-support-nonce' ) ):
            $response = array('success' => false, 'message' => 'There seems to be an issue with the security of this form.');
            wp_send_json( $response );
        endif;

        $body = isset($_POST['message']) ? wpautop( $_POST['message'] ) : false;
        if( !$body ):
            $response = array('success' => false, 'message' => 'Please let us know why you are contacting us in Your Message.');
            wp_send_json( $response );
        endif;

        $from = isset($_POST['author_name']) ? sanitize_text_field( $_POST['author_name'] ) : false;
        $from_email = isset($_POST['author_email']) ? sanitize_email( $_POST['author_email'] ) : false;
        $install = isset($_POST['install']) ? sanitize_text_field( $_POST['install'] ) : false;

        if( !$from || !$from_email || !$install ):
            $response = array('success' => false, 'message' => 'The form cannot be sent due to key information being missing. Please try again.');
            wp_send_json( $response );
        endif;

        $subject = isset($_POST['subject']) ? sanitize_text_field( $_POST['subject'] ) : 'Support Request for ' . $install;
        $body = 'Support for Install: ' . $install . '<br />Request by: '. $from .' <'. $from_email .'><br /><br />' . $body;

        $headers = [];
        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        $headers[] = 'From: ' . $from . ' <' . $from_email . '>';
        $headers[] = 'Reply-To: ' . $from . ' <' . $from_email . '>';

        if( ! wp_mail( Properties::getInstance()->get( Properties::KEY_SUPPORT_EMAIL), $subject, $body, $headers ) ):
            $response = array('success' => false, 'message' => 'Something went wrong with sending your message. Please try again.');
        else:
            $response = array('success' => true, 'message' => 'Thank you for your message!<br />We try to respond to all inquires within 1 business day.');
        endif;

        wp_send_json( $response );
    }

    /**
     * Setup our custom settings fo the Support Dashboard
     *
     * @param array $settings
     *
     * @return array mixed
     */
    public function setMaintenanceDashboardSettings( $settings ){
        // Enable Dashboard white labeling if it isn't already
        if( ! get_option( 'support_enable_whitelabel' ) ) {
            update_option( 'support_enable_whitelabel', 1 );
        }

        // Set our defaults for white labeling

//        $settings['Menu'] = 'Maintenance';
//        $settings['Icon'] = 'dashicons-sos';
//        $settings['Title'] = '';
        $settings['Logo']   = Plugin::getInstance()->getURL('assets/imgs/maintenance-dash-logo.png');
//        $settings['Name']   = __('Watchdog Studio Maintenance Dashboard', 'watchdog');
        $settings['Author'] = 'Watchdog Studio';
        $settings['Description'] = __('Provides functionality related to your Watchdog Studio website care subscription.', 'watchdog');
        $settings['Hide'] = true;
        if( Permissions::getInstance()->isSuperUser() ) {
            $settings['Hide'] = false;
        }

        return $settings;
    }

    /**
     * Disable WP Engine News
     */
    public function disableWPENews(){
        if( get_site_option( 'wpengine_news_feed_enabled', 1 ) ):
            update_site_option( 'wpengine_news_feed_enabled', 0 );
        endif;

        if( get_site_option( 'wpengine_admin_bar_enabled', 1 ) ):
            update_site_option( 'wpengine_admin_bar_enabled', 0 );
        endif;
    }
}