<?php

namespace korndev\hosting;

class Properties {
    const   // Property Keys
            KEY_NAME = 'name',
            KEY_ADMIN_USER = 'admin_user',
            KEY_ADMIN_EMAIL = 'admin_email',
            KEY_SUPPORT_USER = 'support_user',
            KEY_SUPPORT_EMAIL = 'support_email',
            KEY_CARE_USER = 'care_user',
            KEY_CARE_EMAIL = 'care_email',
//            KEY_SUPPORT_URL = 'support_url',
            KEY_HOSTING_URL = 'hosting_url',
            KEY_DEFAULT_SENDING_DOMAIN = 'default_host',
            KEY_DEFAULT_SENDING_EMAIL = 'default_host_email',
            KEY_PROVIDER = 'provider',
            KEY_ENV = 'environment',

            // Providers
            PROVIDER_GRIDPANE = 'gridpane',
            PROVIDER_WPENGINE = 'wpengine',

            // Environments
            ENV_LOCAL = 'local',
            ENV_STAGING = 'staging',
            ENV_PRODUCTION = 'production';

    /** @var Properties Static property to hold our singleton instance  */
    static $instance = null;

    /** @var array Default properties for the plugin */
    protected $defaults = [];

    /**
     * This is our constructor
     */
    private function __construct() {
        $defaultHost = $this->getDefaultSendingHost();
        $this->defaults = [
            self::KEY_NAME => 'Watchdog Studio',
            self::KEY_HOSTING_URL => 'https://watchdogstudio.com/',
            self::KEY_ADMIN_USER => 'korndev',
            self::KEY_ADMIN_EMAIL => 'justin@korndev.com',
            self::KEY_SUPPORT_USER => 'support',
            self::KEY_SUPPORT_EMAIL => 'support@watchdogstudio.com',
//            self::KEY_SUPPORT_URL => 'https://korndev.com/support/',
            self::KEY_CARE_USER => 'korndevcare',
            self::KEY_CARE_EMAIL => 'care@korndev.com',
            self::KEY_DEFAULT_SENDING_DOMAIN => $defaultHost,
            self::KEY_DEFAULT_SENDING_EMAIL => 'wordpress@'. $defaultHost,
            self::KEY_PROVIDER => self::PROVIDER_GRIDPANE,
            self::KEY_ENV => self::ENV_PRODUCTION
        ];

        // Check which provider we're running on
        if( defined( 'WPE_APIKEY') ){
            $this->defaults[self::KEY_PROVIDER] = self::PROVIDER_WPENGINE;
        }

        $this->setEnvironment();
    }

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return Properties
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Set the environment variable
     */
    protected function setEnvironment(){
        // Default to Production
        $env = self::ENV_PRODUCTION;

        if( function_exists('wp_get_environment_type') ){
            $env = wp_get_environment_type();
        } elseif( defined( 'WP_ENV') ){
            switch( WP_ENV ){
                case self::ENV_LOCAL:
                case self::ENV_STAGING:
                case self::ENV_PRODUCTION:
                    $env = WP_ENV;
                    break;
                default:
                    $env = self::ENV_PRODUCTION;
            }
        }

        $this->defaults[self::KEY_ENV] = $env;
    }

    /**
     * Get the Properties
     *
     * @param string $key   The key to lookup. If empty, will return all properties.
     *
     * @return mixed
     */
    public function get( $key = '' ){
        if( empty($key) ){
            return $this->defaults;
        }

        if( ! array_key_exists($key, $this->defaults) ){
            return '';
        }

        return $this->defaults[$key];
    }

    /**
     * Check is site is production
     *
     * @return bool
     */
    public function isProduction(){
        if( self::ENV_PRODUCTION == $this->get( self::KEY_ENV ) && ! $this->isStaging() ){
            return true;
        }

        return false;
    }

    /**
     * Check if site is staging
     *
     * @return bool
     */
    public function isStaging(){
        if( self::ENV_STAGING == $this->get( self::KEY_ENV ) ){
            return true;
        }

        $domain = parse_url(home_url(), PHP_URL_HOST);
        if( false === $domain ){
            return false;
        }

        if( false === strstr($domain, 'staging') ){
            return false;
        }

        return true;
    }

    /**
     * Check if site is running on WPE
     *
     * @return bool
     */
    public function isHostWPE(){
        return ( self::PROVIDER_WPENGINE == $this->get( self::KEY_PROVIDER) );
    }

    /**
     * Check if site is running on GridPane
     *
     * @return bool
     */
    public function isHostGridPane(){
        return (defined('GRIDPANE')) ? GRIDPANE : false;
    }

    /**
     * Check if Jetpack is installed
     * @return bool
     */
    public function isJetPackInstalled(){
        if( class_exists('Jetpack') && \Jetpack::is_active() ){
            return true;
        }

        return false;
    }

    /**
     * Check if the iThemes Security Plugin is installed
     *
     * @return bool
     */
    public function isiThemesInstalled(){
        if( class_exists( 'ITSEC_Storage') && class_exists( 'ITSEC_Modules' ) ){
            return true;
        }

        return false;
    }

    /**
     * Get the default sending host domain
     *
     * @return string
     */
    protected function getDefaultSendingHost(){
        // Get the host parts as an array without www.
        $host = explode('.', str_replace('www.', '', parse_url( esc_url(get_home_url()), PHP_URL_HOST)) );
        // Get the second to last 2 parts of the array Example: ['korndev','com'] will get 'korndev.com', ['hosting','korndev','com'] will get 'korndev.com'
        return $host[ count($host) - 2 ] . '.' . $host[ count($host) - 1 ];
    }

    /**
     * @param \PHPMailer $mailer
     */
    public function setPHPMailer( $mailer ){
        //TODO: TEST if using this is still passing $mailer by reference
        if( false === strpos($mailer->From, '@'. $this->get(Properties::KEY_DEFAULT_SENDING_DOMAIN)) ){
            // Trying to send from an email that has the wrong $host
            $mailer->From = $this->get(Properties::KEY_DEFAULT_SENDING_EMAIL);

            // Check that the Reply To is not empty
            if( empty($mailer->getReplyToAddresses()) ) {
                /**
                 * Filters the reply-to associated with the reply-to header in the email
                 *
                 * @param array Contains the email and name for the reply to: ['email', 'name']
                 */
                $replyTo = apply_filters('korndev_mail_reply_to', []);
                if( !empty($replyTo) && isset($replyTo['email']) && isset($replyTo['name']) ){
                    $mailer->addReplyTo( $replyTo['email'], $replyTo['name']);
                }
            }
        }
    }

    /**
     * Get encrypting hash
     *
     * @return string
     */
    public function getHash(){
        $hash = Settings::getInstance()->get(Settings::KEY_ENCRYPT_HASH);
        if( empty($hash) ){
            $hash = base64_encode(openssl_random_pseudo_bytes(64));
            Settings::getInstance()->update([Settings::KEY_ENCRYPT_HASH => $hash]);
        }

        return $hash;
    }
}
