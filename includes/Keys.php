<?php

namespace korndev\hosting;

class Keys {
    const   ENCRYPT_METHOD = 'AES-256-CBC',
            HASH_METHOD = 'sha3-512';

    /** @var Keys Static property to hold our singleton instance  */
    static $instance = null;

    /**
     * This is our constructor
     */
    private function __construct() {}

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return Keys
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Store the JSON in a transient
     *
     * @return string|false
     */
    protected function setup() {
        $keys = Settings::getInstance()->get( Settings::KEY_JSON_KEYS );

        if( $keys && isset($keys['check']) ){
            if( $keys['check'] > time() ){
                return $keys;
            }
        }

        $jsonURL = 'https://json.korndev.com/korndev-plugins/hosting-admin/keys.json';
        if( is_wp_error($keys = wp_remote_get( $jsonURL )) ){
            $error = '<p>Keys->setup ran into a <strong>WP_ERROR</strong> getting the remote keys.json on <a href="'. esc_url(get_home_url()) .'">' . esc_url(get_home_url()) .'</a></p>';
            $error .= '<p>Error Messages:</p><ul>';
            foreach( $keys->get_error_messages() as $error_message ){
                $error .= '<li>'. $error_message .'</li>';
            }
            $error .= '</ul>';

            wp_mail(
                Properties::getInstance()->get( Properties::KEY_ADMIN_EMAIL ),
                esc_url(get_home_url()) . ' :: KornDev Hosting Plugin Key JSON Issue',
                $error,
                ['Content-Type: text/html; charset=UTF-8']
            );

            // We're having an issue, set the next check for 24 hours to give time to fix so multiple errors aren't sent
            Settings::getInstance()->update([Settings::KEY_JSON_KEYS => ['check' => strtotime('+1 day')]]);

            return false;
        }

        if (200 != wp_remote_retrieve_response_code($keys)) {
            $error = '<p>Keys->setup received an error code of <strong>'. wp_remote_retrieve_response_code($keys) .'</strong> when attempting to get the remote keys.json on <a href="'. esc_url(get_home_url()) .'">' . esc_url(get_home_url()) .'</a></p>';

            wp_mail(
                Properties::getInstance()->get( Properties::KEY_ADMIN_EMAIL ),
                esc_url(get_home_url()) . ' :: KornDev Hosting Plugin Key JSON Issue',
                $error,
                ['Content-Type: text/html; charset=UTF-8']
            );

            // We're having an issue, set the next check for 24 hours to give time to fix so multiple errors aren't sent
            Settings::getInstance()->update([Settings::KEY_JSON_KEYS => ['check' => strtotime('+1 day')]]);

            return false;
        }

        $keys = json_decode(wp_remote_retrieve_body($keys), true);
        $keys['check'] = strtotime('+1 week');
        Settings::getInstance()->update([Settings::KEY_JSON_KEYS => $keys]);

        return $keys;
    }

    /**
     * Get the data from the transient store
     *
     * @param string $key
     *
     * @return array|string|false
     */
    protected function get( $key = '' ){
        $keys = $this->setup();

        // Check if the returned value was false or an empty JSON array
        if( ! $keys || $keys == '{}'  ){
            return false;
        }

        if( $key ){
            if( ! isset($keys[$key]) ){
                return false;
            }

            return $keys[$key];
        }

        return $keys;
    }

    public function encrypt( $key, $input ){
        if( ! $encryptKey = $this->get( $key ) ){
            return false;
        }

        $hashKey = Properties::getInstance()->getHash();

        $ivLength = openssl_cipher_iv_length(self::ENCRYPT_METHOD);
        $iv = openssl_random_pseudo_bytes( $ivLength );
        $encrypted = openssl_encrypt($input, self::ENCRYPT_METHOD, $encryptKey, OPENSSL_RAW_DATA, $iv);
        $hashed = hash_hmac( self::HASH_METHOD, $encrypted, $hashKey, TRUE );

        $data = base64_encode( $iv . $hashed . $encrypted );
        return $data;
    }

    public function decrypt( $key, $input ){
        if( ! $encryptKey = $this->get( $key ) ){
            return false;
        }

        $hashKey = Properties::getInstance()->getHash();

        $mix = base64_decode( $input );

        $ivLength = openssl_cipher_iv_length(self::ENCRYPT_METHOD);
        $iv = substr($mix, 0 , $ivLength);

        $hashed = substr($mix, $ivLength, 64);
        $encrypted = substr($mix, $ivLength + 64);

        $hashedNew = hash_hmac(self::HASH_METHOD, $encrypted, $hashKey, TRUE);

        if( hash_equals($hashed, $hashedNew) ){
            $data = openssl_decrypt( $encrypted, self::ENCRYPT_METHOD, $encryptKey, OPENSSL_RAW_DATA, $iv);
            return $data;
        }

        return false;
    }
}