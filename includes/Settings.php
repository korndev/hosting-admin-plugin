<?php

namespace korndev\hosting;

class Settings {
    const   OPTION_NAME = Plugin::SLUG . '_plugin',

            KEY_PLUGIN_VERSION          = 'plugin_version',
            KEY_DATABASE_VERSION        = 'database_version',
            KEY_OUTDATED_PLUGIN_CHECK   = 'outdated_plugin_check',
            KEY_JSON_KEYS               = 'json_keys',
            KEY_ENCRYPT_HASH            = 'key_hash',
            KEY_NGNIX_HELPER_SET        = 'ngnix_helper_defaults_set',  // Ngnix Helper
            KEY_PROJECT_HUDDLE_SET      = 'project_huddle_defaults_set', // Project Huddle Child
            KEY_SECURITY_SET            = 'security_defaults_set',  // iThemes Security
            KEY_IMAGIFY_SET             = 'imagify_defaults_set',
            KEY_EMAIL_SMTP              = 'transactional_email_smtp';

    /** @var Settings Static property to hold our singleton instance  */
    static $instance = null;

    /** @var array Accepted plugin setting keys */
    protected $keys = [
        self::KEY_PLUGIN_VERSION,
        self::KEY_DATABASE_VERSION,
        self::KEY_OUTDATED_PLUGIN_CHECK,
        self::KEY_JSON_KEYS,
        self::KEY_ENCRYPT_HASH,
        self::KEY_NGNIX_HELPER_SET,
        self::KEY_PROJECT_HUDDLE_SET,
        self::KEY_SECURITY_SET,
        self::KEY_IMAGIFY_SET,
        self::KEY_EMAIL_SMTP,
    ];

    /**
     * This is our constructor
     */
    private function __construct() {}

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return Settings
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Get the Option Settings from DB
     *
     * @param string    $key        The key to lookup. If empty, will return all options.
     * @param bool      $network    When set to true, retrieve the option/setting from the network vs the site. Default: false
     *
     * @return mixed
     */
    public function get( $key = '', $network = false ){
        if( $network ){
            $options = get_site_option( self::OPTION_NAME, [] );
        } else {
            $options = get_option( self::OPTION_NAME, [] );
        }

        if( empty($key) ){
            return $options;
        }

        if( ! array_key_exists($key, $options) ){
            return '';
        }

        return $options[$key];
    }

    /**
     * Reset (Delete) the Option Settings from DB
     */
    public function reset(){
        delete_site_option( self::OPTION_NAME );
        delete_option(self::OPTION_NAME );
    }

    /**
     * Delete/Reset a specific value from the settings
     *
     * @param string    $key
     * @param bool      $network     When set to true, delete the option/setting from the network vs the site. Default: false
     *
     * @return bool
     */
    public function delete( $key, $network = false ){
        $option_values = $this->get('', $network );
        if( ! isset($option_values[$key]) ){
            return true;
        }

        unset($option_values[$key]);

        if( $network ){
            return update_site_option( self::OPTION_NAME, $option_values );
        }

        return update_option( self::OPTION_NAME, $option_values, true );
    }

    /**
     * Updates the Settings in DB
     *
     * @param array     $new_values     The values to be added/updated in the
     * @param bool      $network        When set to true, update the option/setting for the network vs the site. Default: false
     *
     * @return bool     False if value was not updated and true if value was updated.
     */
    public function update( $new_values, $network = false ){
        // Get the current values
        $old_values = $this->get('', $network);
        if( ! empty($old_values) ) {
            // We don't want to lose values that might exist that aren't being updated
            $new_values = array_merge( $old_values, $new_values );
        }

        // Make sure we only add values for keys we trust
        foreach( $new_values as $key => $value ){
            if( !in_array( $key, $this->keys ) ){
                unset( $new_values[$key] );
            }
        }

        $update = false;
        if( ! empty($old_values) ) {
            foreach ( $new_values as $key => $value ) {
                // If the key doesn't exist in the old values, we definitely have to update
                if( ! array_key_exists($key, $old_values) ){
                    $update = true;
                    break;
                }

                // We only have to update if the old values and new values don't match
                if( is_array($value) ){
                    if( $old_values[$key] !== $value ){
                        $update = true;
                        break;
                    }
                } elseif ( (string)$old_values[$key] != (string)$value ) {
                    // We compare string values to be sure we don't have discrepancy with numbers
                    $update = true;
                    break;
                }
            }
        } else {
            // Old values do not exist yet, we need to update
            $update = true;
        }

        // Check if we need to update the DB
        if( $update ){
            // Update the DB
            if( $network ){
                $updated = update_site_option( self::OPTION_NAME, $new_values );
            } else {
                $updated = update_option(self::OPTION_NAME, $new_values, true );
            }

            if( ! $updated  ) {
                error_log('KornDev Hosting Admin :: Failed to update settings in the database.');
            }

            return true;
        }

        // $update is false
        return false;
    }

}
