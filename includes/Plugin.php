<?php

namespace korndev\hosting;

use korndev\hosting\Plugins\Fail2Ban;
use korndev\hosting\Plugins\Imagify;
use korndev\hosting\Plugins\iThemesSecurity;
use korndev\hosting\Plugins\NgnixHelper;
use korndev\hosting\Plugins\Postmark;
use korndev\hosting\Plugins\ProjectHuddle;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Plugin {

    const   SLUG = 'korndev_hosting_admin';

    /** @var Plugin Static property to hold our singleton instance  */
    private static $instance = null;

    /** @var bool $isStaging */
    protected $isStaging;

    /**
     * Constructor
     */
    public function __construct() {
        $this->loadDependencies();
        $this->init();

        do_action( self::SLUG . '_loaded' );
    }

    /**
     * Creates or returns an instance of this class.
     *
     * @return Plugin|null
     */
    public static function getInstance()
    {
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Load plugin dependencies
     */
    protected function loadDependencies(){
        require $this->getPath( 'resources/plugin-update-checker/plugin-update-checker.php' );
        require_once $this->getPath( 'includes/Keys.php' );
        require_once $this->getPath( 'includes/Admin.php' );
        require_once $this->getPath( 'includes/Permissions.php' );
        require_once $this->getPath( 'includes/Properties.php' );
        require_once $this->getPath( 'includes/Settings.php' );
        require_once $this->getPath( 'includes/Versions.php' );
        require_once $this->getPath( 'includes/Plugins/ProjectHuddle.php' );
        require_once $this->getPath( 'includes/Plugins/NgnixHelper.php' );
        require_once $this->getPath( 'includes/Plugins/Imagify.php' );
        require_once $this->getPath( 'includes/Plugins/iThemesSecurity.php' );
        require_once $this->getPath( 'includes/Plugins/SendGrid.php' );
        require_once $this->getPath( 'includes/Plugins/Postmark.php' );
//        require_once $this->getPath( 'includes/Plugins/WPRocket.php' );
        require_once $this->getPath( 'includes/Plugins/Fail2Ban.php' );
    }

    /**
     * Initialize the plugin
     */
    protected function init(){
        //TODO: Check if everything required is defined and available

        // Where we setup the environment and provider and other details
        Properties::getInstance();

        $this->isStaging = Properties::getInstance()->isStaging();

        // Setup our Actions and Filters
        $this->globalHooks();
        if (is_admin()) {
            $this->adminHooks();
        } else {
            $this->frontHooks();
        }

    }

    /**
     * Get the plugin slug
     *
     * @return string
     */
    public function getSlug(){
        return self::SLUG;
    }

    /**
     * The path of the plugin.
     *
     * @param string $path Optional path within the plugin itself.
     *
     * @return string
     */
    public function getPath( $path = null ) {
        if ( ! empty( $path ) ) {
            return plugin_dir_path( dirname( __FILE__ ) ) . $path;
        } else {
            return plugin_dir_path( dirname( __FILE__ ) );
        }
    }

    /**
     * The URL of the plugin.
     *
     * @param string $path Optional path within the plugin itself.
     *
     * @return string
     */
    public function getURL( $path = null ) {
        if ( ! empty( $path ) ) {
            return plugin_dir_url( dirname( __FILE__ ) ) . $path;
        } else {
            return plugin_dir_url( dirname( __FILE__ ) );
        }
    }

    /**
     * Setup necessary Actions and Filters for the plugin
     */
    protected function globalHooks(){
        remove_action('phpmailer_init', 'use_smtp_email' );
        add_action( 'phpmailer_init', [Postmark::getInstance(), 'phpMailer'] );

        add_action( 'init', [Versions::getInstance(), 'checkUpdate'] );
        add_action( 'init', [Versions::getInstance(), 'checkPlugins'] );
        add_action( 'wp_before_admin_bar_render', [Admin::getInstance(), 'filterAdminBar'] );

        add_filter( 'after_setup_theme', [$this, 'finalizeSetup'], 100 );

        // Control WP Auto Updates
        if( ! Properties::getInstance()->isHostWPE() ) {
            $autoUpdateDisabled = '__return_true';
            if( $this->isStaging ){
                $autoUpdateDisabled = '__return_false';
            }

            add_filter( 'automatic_updater_disabled', $autoUpdateDisabled, 100 );
            add_filter( 'auto_core_update_email', [$this, 'autoUpdateEmails'], 10, 4);
        }


        if( $this->isStaging ) {
            // Make sure any WooCommerce Subscriptions are always in "duplicate site" mode on staging
            add_filter( 'woocommerce_subscriptions_is_duplicate_site', '__return_true' );
        }

        // Enable JetPack DEV mode on staging and locally
        if( $this->isStaging || (defined('WP_ENV') && WP_ENV == 'local') ){
            add_filter('jetpack_development_mode', '__return_true');
        }

        // Disable Application Passwords in WordPress 5.6+
        if( !defined('WATCHDOG_ENABLE_APP_PASSWORDS') || true !== WATCHDOG_ENABLE_APP_PASSWORDS ){
            add_filter( 'wp_is_application_passwords_available', '__return_false' );
        }

        /** Example Reply-To filter for SendGrid */
        /*
        add_filter( 'korndev_mail_reply_to', function( $replyTo ){
            return ['justin@korndev.com', 'Justin Korn'];
        });
        */

        if( ! Permissions::getInstance()->isSuperUser( true ) ) {
            // Remove CleanTalk Buttons
            remove_action('manage_comments_nav', 'apbct_add_buttons_to_comments_and_users', 10);
            remove_action('manage_users_extra_tablenav', 'apbct_add_buttons_to_comments_and_users', 10);
        }
    }

    /**
     * Setup necessary Actions and Filters for the plugin frontend
     */
    protected function frontHooks(){

    }

    /**
     * Setup necessary Actions and Filters for the plugin backend
     */
    protected function adminHooks(){
        // Actions
        add_action( 'admin_init', [Admin::getInstance(), 'adminInit'] );
        add_action( 'wp_dashboard_setup', [Admin::getInstance(), 'dashboardWidgets'] );
        add_action( 'admin_menu', [Admin::getInstance(), 'removeMenus'], 999 );
        add_action( 'network_admin_menu', [Admin::getInstance(), 'removeMenus'], 999 );
        add_action( 'wp_ajax_korndev_hosting_supportMessage', [Admin::getInstance(), 'sendSupportMessage'] );
        add_action( 'wp_ajax_korndev_hosting_purgeCache', [Admin::getInstance(), 'purgeCache'] );

        if( Properties::getInstance()->isHostWPE() ) {
            add_action( 'admin_init', [Admin::getInstance(), 'disableWPENews'], 5 );
            add_action( 'pre_user_query', [Admin::getInstance(), 'filterUserList'] );

            define('WPE_POPUP_DISABLED', true);     // Disable WPE plugin update notifications
        }

        // Filters
        add_filter( 'the_generator', '__return_empty_string' ); // Remove the WordPress generator tag from displaying in the source
        add_filter( 'admin_footer_text', [Admin::getInstance(), 'footer'] );
        add_filter( 'show_advanced_plugins', [Admin::getInstance(), 'showMustUse'] );
        add_filter( 'all_plugins', [Admin::getInstance(), 'filterPluginList'], 999 );
        add_filter( 'support_branding', [Admin::getInstance(), 'setMaintenanceDashboardSettings'], 999 );

        // Disable plugins auto-update UI elements.
        add_filter( 'plugins_auto_update_enabled', '__return_false' );
        // Disable themes auto-update UI elements.
        add_filter( 'themes_auto_update_enabled', '__return_false' );
        // Disable core auto-updates
        if( Properties::getInstance()->isProduction() ) {
            if( ! (defined('WP_AUTO_UPDATE_CORE')) ){
                define('WP_AUTO_UPDATE_CORE', false);
            }
            add_filter( 'allow_major_auto_core_updates', '__return_false' );
            add_filter( 'allow_minor_auto_core_updates', '__return_false' );
        }

        //TODO: Move to Security class
        if( !defined( 'ITSEC_DISABLE_INACTIVE_USER_CHECK' ) ){
            // Disable iThemes Inactive User Warning notices
            define( 'ITSEC_DISABLE_INACTIVE_USER_CHECK', true );
        }

        add_action( 'admin_init', [Imagify::getInstance(), 'init'] );
        add_action( 'init', [Fail2Ban::getInstance(), 'init'] );
    }

    /**
     * Setup 3rd Party plugin settings
     */
    public function finalizeSetup(){
        //TODO: Move to WP Rocket class ?
//        if( Permissions::getInstance()->isSuperUser( true, true ) ) {
//            define('WP_RWL', true);     // White label WP-Rocket
//        }

        Postmark::getInstance()->init();
        ProjectHuddle::getInstance()->setDefaults();
        NgnixHelper::getInstance()->setDefaults();
//        WPRocket::getInstance()->whiteLabel();
        Imagify::getInstance()->setDefaults();
        iThemesSecurity::getInstance()->setDefaults();
    }

    /**
     * Control who retrieves the auto update emails
     *
     * @param array $email          Array of email arguments [ $to, $subject, $body, $headers ]
     * @param string $type          The type of email being sent. Can be one of 'success', 'fail', 'manual', 'critical'.
     * @param object $core_update   The update offer that was attempted.
     * @param mixed  $result        The result for the core update. Can be WP_Error.
     *
     * @return array
     */
    public function autoUpdateEmails( $email, $type, $core_update, $result ){
        $email['to'] = Properties::getInstance()->get( Properties::KEY_ADMIN_EMAIL );
        return $email;
    }
}