<?php

namespace korndev\hosting;

class Versions {

    /** @var Versions Static property to hold our singleton instance  */
    static $instance = null;

    /** @var array Plugin versions and their upgrade methods */
    protected $versions = [
        '0.1.0' => 'v010',
        '4.0.4' => 'v404',
        '4.1.0' => 'v410',
        '4.1.16' => 'v4116',
        // If there are actions that need to be taken due to a version update,
        // create a method with the same name as the value in the array.
        // See function v010 as example.
    ];

    /**
     * If an instance exists, this returns it.  If not, it creates one and returns it.
     *
     * @return Versions
     */
    public static function getInstance() {
        if ( ! self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function getAll(){
        return $this->versions;
    }

    /**
     * Get the plugin version
     *
     * @return string
     */
    public function getCurrent(){
        // Move to the end of the version array (which should contain the current version)
        end($this->versions );
        // Get the key/version of the DB
        $version = key( $this->versions );
        // Reset the version array to the beginning
        reset( $this->versions );

        return $version;
    }

    /**
     * Check the plugin version and update/refresh as needed
     */
    public function checkUpdate(){
        $settings = Settings::getInstance()->get();

        if (
            empty($settings) ||
            ! array_key_exists(Settings::KEY_PLUGIN_VERSION, $settings) ||
            empty($settings[Settings::KEY_PLUGIN_VERSION]) ||
            version_compare( $settings[Settings::KEY_PLUGIN_VERSION], $this->getCurrent(), '<' )
        ){
            // Run setup/update methods
            foreach ( $this->versions as $version => $method ) {
                if ( is_array($settings) && array_key_exists(Settings::KEY_PLUGIN_VERSION, $settings) && version_compare( $settings[Settings::KEY_PLUGIN_VERSION], $version, '<' ) ) {
                    if( method_exists($this, $method) ) {
                        call_user_func([ $this, $method ]);
                    }
                }
            }

            // Update the DB setting value
            Settings::getInstance()->update( [Settings::KEY_PLUGIN_VERSION => $this->getCurrent()] );
        }
    }

    /**
     * Check installed plugins for abandoned, disabled and pending updates
     */
    public function checkPlugins(){
        // Only run when wp-admin is loaded, CRON is running or CLI is running
        if( ! is_admin() && ! ( defined( 'DOING_CRON' ) && DOING_CRON ) && ! ( defined( 'WP_CLI') && WP_CLI ) ) {
            return;
        }

        // TODO: Test with Multisite
        if ( is_multisite() ){
            return;
        }

        $settings = Settings::getInstance()->get();

        // Check if it's time to run
        $nextRun = isset($settings[Settings::KEY_OUTDATED_PLUGIN_CHECK]) ? (int)$settings[Settings::KEY_OUTDATED_PLUGIN_CHECK] : false;
        if( $nextRun && $nextRun > time() ){
            return;
        }

        // Set the key so we don't run again for another month
        Settings::getInstance()->update( [Settings::KEY_OUTDATED_PLUGIN_CHECK => strtotime('+1 month') ] );

        try {
            $abandonedMonthsAllowed = 12;

            $pluginsNotFound  = [];
            $pluginsAbandoned = [];
            $pluginsDisabled = [];

            /**
             * Connect to WordPress.org using plugins_api
             * About plugins_api -
             * http://wp.tutsplus.com/tutorials/plugins/communicating-with-the-wordpress-org-plugin-api/
             */
            if ( ! function_exists('get_plugins')) {
                require_once ABSPATH . 'wp-admin/includes/plugin.php';
            }

            if ( ! function_exists('plugins_api')) {
                require_once ABSPATH . 'wp-admin/includes/plugin-install.php';
            }

            // Slugs of plugins that we shouldn't check because we know they're good
            $skipPlugins = [
                'action-scheduler/action-scheduler.php',
                'cleantalk-spam-protect/cleantalk.php',
                'gridpane-redis-object-cache/gridpane-redis-object-cache.php',
                'korndev-hosting-admin/korndev-hosting-admin.php',
                'support-dashboard/support-dashboard.php',
                'nginx-helper/nginx-helper.php',
                'projecthuddle-child-site/ph-child.php',
                'redis-cache/redis-cache.php',
                'wp-fail2ban/wp-fail2ban.php',
                'worker/init.php'
            ];

            $plugins = get_plugins();
            $activePlugins = get_option('active_plugins');
            foreach ($plugins as $key => $plugin) {
                // Skip some plugins
                if( in_array($key, $skipPlugins) ){
                    continue;
                }

                // Check for disabled plugins
                if( !in_array($key, $activePlugins) ){
                    $pluginsDisabled[$key] = ['name' => $plugin['Name']];
                }

                // Get the plugin slug
                $slug = dirname(plugin_basename($key));

                // Get the plugin details
                $pluginInfo = plugins_api('plugin_information', array('slug' => $slug));

                // Check for errors with the data returned from WordPress.org
                if ( ! $pluginInfo || is_wp_error($pluginInfo)) {
                    // If the plugin is disabled, no need to repeat it in the not found list
                    if( ! array_key_exists($key, $pluginsDisabled) ) {
                        $pluginsNotFound[] = $plugin;
                    }
                    continue;
                }

                // Check for abandoned plugins

                // Check if last updated is over 50 years ago, likely not correct and a "not found" plugin
                $diff = time() - strtotime($pluginInfo->last_updated);
                if ($diff >= 50 * YEAR_IN_SECONDS) {
                    $pluginsNotFound[] = $plugin;
                    continue;
                }

                // Check if the plugin is abandoned
                $oldest = time() - $abandonedMonthsAllowed * MONTH_IN_SECONDS;
                if (strtotime($pluginInfo->last_updated) <= $oldest) {
                    $pluginsAbandoned[] = $pluginInfo;
                }
            }

            $installURL  = esc_url(get_site_url());
            $installHost = parse_url($installURL, PHP_URL_HOST);

            $headers   = [];
            $headers[] = 'Content-Type: text/html; charset=UTF-8';

            if ( ! empty($pluginsAbandoned)) {
                // We have out dated plugins, notify support
                $subject = $installHost . ' :: Abandoned Plugins Detected';
                $body    = '<p>Our plugin monitor has detected abandoned plugins installed on ' . $installHost . '. Running code that has not been updated in over ' . $abandonedMonthsAllowed . ' months is far from ideal and could be a security issue. Did you know 98% of WordPress vulnerabilities are related to plugins?</p>';
                $body    .= '<p>Please review the below plugins to determine if they can be removed or if they need to be replaced. If you need assistance determining if a plugin can be removed and/or finding a replacement, we are happy to help. Or, if you work with someone that knows your website inside and out (like a website developer), you can forward this email on them and they should know what to do.</p>';
                $body    .= '<h4 style="text-transform: uppercase">Plugins not updated in the past ' . $abandonedMonthsAllowed . ' months</h4>';
                $body    .= '<table cellpadding="0" cellspacing="10"><tr><td><span style="font-weight: bold;">Plugin Name</span></td><td><span style="font-weight: bold;">Last Updated</span></td></tr>';
                foreach ($pluginsAbandoned as $plugin) {
                    $body .= '<tr><td>' . $plugin->name . '</td><td>' . human_time_diff(strtotime($plugin->last_updated), time()) . ' ago</td></tr>';
                }
                $body .= '</table>';

                if ( ! empty($pluginsNotFound)) {
                    $body .= '<h4 style="text-transform: uppercase">Plugins not found in WordPress repository</h4>';
                    $body .= '<p>These should all be premium plugins that you have a license for and are currently being maintained outside of the WordPress repository. If not, they should be removed and/or replaced.</p>';
                    $body    .= '<table cellpadding="0" cellspacing="10"><tr><td><span style="font-weight: bold;">Plugin Name</span></td><td><span style="font-weight: bold;">URI</span></td></tr>';
                    foreach ($pluginsNotFound as $plugin) {
                        $uri  = (isset($plugin['PluginURI']) && ! empty($plugin['PluginURI'])) ? '<a href="' . $plugin['PluginURI'] . '">' . $plugin['PluginURI'] . '</a>' : 'N/A';
                        $body .= '<tr><td>' . $plugin['Title'] . '</td><td>' . $uri . '</td></tr>';
                    }
                    $body .= '</table>';
                }

                if( !empty($pluginsDisabled) ){
                    $body .= '<h4 style="text-transform: uppercase">Disabled Plugins</h4>';
                    $body .= '<p>These plugins are installed but not active on your website. If they are not being used, they should be removed.</p>';
                    $body .= '<table cellpadding="0" cellspacing="10">';
                    foreach($pluginsDisabled as $plugin){
                        $body .= '<tr><td>' . $plugin['name'] .'</td></tr>';
                    }
                    $body .= '</table>';
                }

                $body .= '<p>Please <a href="' . trailingslashit($installURL) . 'wp-admin/plugins.php">login to your website admin</a> and review these plugins ASAP.</p>';

                $body .= '<p>If you you have any questions or concerns, do not hesitate to let us know.</p>';

                // Send to email set as admin email
                $toEmail = get_option('admin_email', Properties::getInstance()->get(Properties::KEY_SUPPORT_EMAIL));
                wp_mail($toEmail, $subject, $body, $headers);
            }
        } catch( \Throwable $e ){
            // To something with error
            return;
        }
    }

    protected function v010(){
        // Initial setup

        // Move legacy korndev_hosting_settings to new settings
        $settings = get_option('korndev_hosting_settings');
        delete_option('korndev_hosting_settings');
        Settings::getInstance()->update( $settings );

        if( is_multisite() ){
            // Move legacy korndev_hosting_settings to new settings
            $settings = get_site_option('korndev_hosting_settings');
            delete_site_option('korndev_hosting_settings');
            Settings::getInstance()->update( $settings, true );
        }
    }

    /**
     * Version 4.1.0
     */
    protected function v410(){
        // Reset SMTP for those that had it setup already
        Settings::getInstance()->delete(Settings::KEY_EMAIL_SMTP );
    }
}